/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 16:21:00 by alangloi          #+#    #+#             */
/*   Updated: 2020/01/02 14:52:43 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char		*ptr;
	unsigned char		ok;

	ptr = (unsigned char *)s;
	ok = (unsigned char)c;
	while (n--)
	{
		if (*ptr == ok)
			return (ptr);
		ptr++;
	}
	return (NULL);
}
