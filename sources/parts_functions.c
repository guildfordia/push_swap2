/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parts_functions.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:29:49 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/11 19:47:06 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	much_parts(int len)
{
	int	nb_splits;

	if (len < 0)
		return (0);
	else if (len <= 50)
		nb_splits = 2;
	else if (len > 50 && len <= 150)
		nb_splits = 5;
	else if (len > 150 && len <= 250)
		nb_splits = 5;
	else if (len > 250 && len <= 350)
		nb_splits = 6;
	else if (len > 350 && len <= 450)
		nb_splits = 8;
	else if (len > 450 && len <= 550)
		nb_splits = 10;
	else
		nb_splits = 12;
	return (nb_splits);
}

int	*allocate_parts(t_stack *a, int nb_splits)
{
	int	*part;

	part = partition(a, nb_splits);
	part[nb_splits] = INT_MAX;
	part[0] = INT_MIN;
	return (part);
}

void	sort_parts(t_stack *a, t_stack *b, int i, int size)
{
	int	m;

	m = 0;
	while (m < size)
	{
		closest_to_top(a, b, i);
		prepare_stack(a, b);
		do_push('b', a, b);
		m++;
	}
}
