/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   closest_to_top.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:28:02 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/11 15:28:06 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static int	find_bot(t_stack *a, t_elem *bot)
{
	t_elem	*tob;
	int		l;

	l = 1;
	tob = bot;
	if (tob == a->last)
		;
	else
	{
		while (tob != a->last)
		{
			l++;
			tob = tob->next;
		}
	}
	return (l);
}

static int	pos_bot(t_stack *a, int i)
{
	int		l;
	t_elem	*bot;

	l = 0;
	bot = a->last;
	if (bot)
	{
		while (bot != NULL)
		{
			if ((bot->value > a->part[i - 1] && bot->value <= a->part[i]))
			{
				l = find_bot(a, bot);
				break ;
			}
			bot = bot->prev;
		}
	}
	return (l);
}

static int	find_top(t_stack *a, t_elem *top)
{
	t_elem	*pot;
	int		k;

	k = 0;
	pot = top;
	if (pot == a->head)
		;
	else
	{
		while (pot != a->head)
		{
			k++;
			pot = pot->prev;
		}
	}
	return (k);
}

static int	pos_top(t_stack *a, int i)
{
	int		k;
	t_elem	*top;

	k = 0;
	top = a->head;
	if (top)
	{
		while (top != NULL)
		{
			if ((top->value > a->part[i - 1] && top->value <= a->part[i]))
			{
				k = find_top(a, top);
				break ;
			}
			top = top->next;
		}
	}
	return (k);
}

void	closest_to_top(t_stack *a, t_stack *b, int i)
{
	int	k;
	int	l;

	k = pos_top(a, i);
	l = pos_bot(a, i);
	if (k < l)
	{
		while (k > 0)
		{
			do_rotate('a', a, b);
			k--;
		}
	}
	else
	{
		while (l > 0)
		{
			do_rev_rotate('a', a, b);
			l--;
		}
	}
}
