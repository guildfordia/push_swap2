/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prepare_stack.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:30:31 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/11 15:30:34 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static int	upper_value(t_stack *a, t_stack *b, int *up, int *is_up)
{
	t_elem	*cur;

	cur = b->head;
	while (cur != NULL)
	{
		if (a->head->value < cur->value)
		{
			if (cur->value < *up)
			{
				*up = cur->value;
				*is_up = 1;
			}
		}
		cur = cur->next;
	}
	return (*up);
}

static int	downer_value(t_stack *a, t_stack *b, int *dw, int *is_dw)
{
	t_elem	*cur;

	cur = b->head;
	while (cur != NULL)
	{
		if (a->head->value > cur->value)
		{
			if (cur->value > *dw)
			{
				*dw = cur->value;
				*is_dw = 1;
			}
		}
		cur = cur->next;
	}
	return (*dw);
}

void	prepare_stack(t_stack *a, t_stack *b)
{
	int	dw;
	int	up;
	int	is_dw;
	int	is_up;

	dw = INT_MIN;
	up = INT_MAX;
	is_dw = 0;
	is_up = 0;
	if (b->head)
	{
		downer_value(a, b, &dw, &is_dw);
		upper_value(a, b, &up, &is_up);
		if (is_dw && is_up)
			smart_rotate(a, b, dw, 'b');
		else
		{
			up = find_biggest(b);
			smart_rotate(a, b, up, 'b');
		}
	}
}
