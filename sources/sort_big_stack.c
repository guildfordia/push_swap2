/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_big_stack.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:31:09 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/11 15:31:11 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static void	final_push(t_stack *a, t_stack *b)
{
	int	biggy ;

	biggy = find_biggest(b);
	smart_rotate(a, b, biggy, 'b');
	while (b->head)
		do_push('a', a, b);
}

static int	get_size(int i, int nb_splits, int len)
{
	int	size;

	size = len / nb_splits;
	if (i == nb_splits)
	{
		if (len % nb_splits > 0)
			size += len % nb_splits;
	}
	return (size);
}

void	sort_big_stack(t_stack *a, t_stack *b)
{
	int	i;
	int	nb_splits;

	i = 1;
	a->len = list_length(a);
	nb_splits = much_parts(a->len);
	a->part = allocate_parts(a, nb_splits);
	while (i <= nb_splits)
	{
		sort_parts(a, b, i, get_size(i, nb_splits, a->len));
		i++;
	}
	final_push(a, b);
	free(a->part);
}
