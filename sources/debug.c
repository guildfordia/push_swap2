/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/30 18:12:41 by alangloi          #+#    #+#             */
/*   Updated: 2021/06/30 18:28:20 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	print_hl(char *str, t_stack *stk)
{
	int	sa;
	int	sl;

	if (stk->head != NULL)
	{
		sa = stk->head->value;
		sl = stk->last->value;
		printf("[%s head = %i]\t[%s last = %i]\n", str, sa, str, sl);
	}
}

void	print_list(char *str, t_stack *stk)
{
	t_elem	*cur;

	cur = stk->head;
	while (cur != NULL)
	{
		printf("%s value = %i\n", str, cur->value);
		cur = cur->next;
	}
}

void	print_op(t_stack *a, t_stack *b)
{
	print_list("a", a);
	print_list("b", b);
	print_hl("a", a);
	print_hl("b", b);
	printf("[[total operations = %i]]\n", a->op);
	printf("---------------\n");
}
