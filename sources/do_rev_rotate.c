/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_rev_rotate.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:28:50 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/11 15:28:53 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static void	rev_rotate(t_elem **head, t_elem **last)
{
	if ((*head) == NULL || (*last) == NULL || (*last)->prev == NULL)
		return ;
	(*head)->prev = (*last);
	(*last)->next = (*head);
	(*head) = (*last);
	(*last) = (*last)->prev;
	(*last)->next = NULL;
	(*head)->prev = NULL;
}

void	do_rev_rotate(char c, t_stack *a, t_stack *b)
{
	if (c == 'a')
	{
		rev_rotate(&a->head, &a->last);
		write(1, "rra", 3);
	}
	else if (c == 'b')
	{
		rev_rotate(&b->head, &b->last);
		write(1, "rrb", 3);
	}
	end_action(a, b);
}
