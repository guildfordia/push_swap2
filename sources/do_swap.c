/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:29:24 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/11 15:29:28 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static void	swap(t_elem **head)
{
	t_elem	*tmp;

	if ((*head) == NULL || (*head)->next == NULL)
		return ;
	tmp = (*head);
	(*head) = (*head)->next;
	tmp->next = (*head)->next;
	tmp->prev = (*head);
	(*head)->next = tmp;
	(*head)->prev = NULL;
	if ((*head)->next->next != NULL)
		(*head)->next->next->prev = (*head)->next;
}

void	do_swap(char c, t_stack *a, t_stack *b)
{
	if (c == 'a')
	{
		swap(&a->head);
		write(1, "sa", 2);
	}
	else if (c == 'b')
	{
		swap(&b->head);
		write(1, "sb", 2);
	}
	end_action(a, b);
}
