/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_list.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:30:15 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/27 14:48:14 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static int	check_elem(char *str, int i, t_stack *a, t_stack *b)
{
	t_elem	*elem;

	if (is_valid(str))
	{
		elem = allocate_elem(ft_atoi(str));
		add_elem_back(a, elem);
		i++;
	}
	else
	{
		end_program(a, b, 1);
	}
	return (i);
}

static int	array_arg(char *argv, t_stack *a, t_stack *b, int l)
{
	char	**tab;
	int		j;

	tab = ps_split(argv, ' ', a, b);
	if (tab)
	{
		j = 0;
		while (tab[j] != NULL)
		{
			l = check_elem(tab[j], l, a, b);
			free(tab[j]);
			tab[j] = NULL;
			j++;
		}
		free(tab);
		tab = NULL;
	}
	else
		end_program(a, b, 1);
	l--;
	return (l);
}

int	fill_list(t_stack *a, t_stack *b, int argc, char **argv)
{
	int	i;
	int	j;

	j = 0;
	i = 1;
	while (i < argc)
	{
		if (ft_strlen(argv[i]) == 0)
			end_program(a, b, 1);
		else if (ft_strchr(argv[i], ' '))
		{
			j = array_arg(argv[i], a, b, j);
			i++;
		}
		else
			i = check_elem(argv[i], i, a, b);
	}
	return (j);
}
