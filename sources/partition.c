/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_stack.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:31:23 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/12 14:34:22 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	*create_array(t_stack *stk)
{
	t_elem	*cur;
	int		i;
	int		*arr;

	i = 0;
	arr = malloc(sizeof(int) * stk->len);
	cur = stk->head;
	while (cur != NULL)
	{
		arr[i] = cur->value;
		i++;
		cur = cur->next;
	}
	return (arr);
}

static void	sort_array(int *tab, int n)
{
	int	i;
	int	j;
	int	tmp;

	i = 0;
	while (i < n)
	{
		j = 0;
		while (j < n - 1)
		{
			if (tab[j] > tab[j + 1])
			{
				tmp = tab[j];
				tab[j] = tab[j + 1];
				tab[j + 1] = tmp;
			}
			j++;
		}
		i++;
	}
}

int	*split_array(int nb_splits, int len, int *tab)
{
	int	*k;
	int	*l;
	int	m;
	int	*part;

	m = 1;
	k = malloc(sizeof(int *) * nb_splits);
	l = malloc(sizeof(int *) * nb_splits);
	part = malloc(sizeof(int *) * nb_splits);
	while (m < nb_splits)
	{
		k[m] = (len * m) / nb_splits;
		l[m] = (m * len) / nb_splits;
		if ((k[m] - l[m]) == 0)
			part[m] = tab[l[m] - 1];
		else
			part[m] = tab[l[m] - 1] + (k[m] - l[m])
				* (tab[l[m]] - tab[l[m] - 1]);
		m++;
	}
	free(k);
	free(l);
	return (part);
}

int	*partition(t_stack *stk, int nb_splits)
{
	int	*part;
	int	*tab;

	tab = create_array(stk);
	sort_array(tab, stk->len);
	part = split_array(nb_splits, stk->len, tab);
	free(tab);
	return (part);
}
