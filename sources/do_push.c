/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_push.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:28:31 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/11 15:28:36 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static void	push(t_elem **dest, t_elem **orig, t_elem **last)
{
	t_elem	*tmp;

	if (!orig)
		return ;
	tmp = (*orig);
	(*orig) = (*orig)->next;
	if ((*orig))
		(*orig)->prev = NULL;
	if ((*dest))
		(*dest)->prev = tmp;
	else
		(*last) = tmp;
	tmp->next = (*dest);
	(*dest) = tmp;
	(*dest)->prev = NULL;
}

void	do_push(char c, t_stack *a, t_stack *b)
{
	if (c == 'a')
	{
		push(&a->head, &b->head, &a->last);
		write(1, "pa", 2);
	}
	else if (c == 'b')
	{
		push(&b->head, &a->head, &a->last);
		write(1, "pb", 2);
	}
	end_action(a, b);
}
