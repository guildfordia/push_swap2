/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_1.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:31:39 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/27 14:49:40 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	is_split(int l, char **dst, t_stack *a, t_stack *b)
{
	int	i;
	int	j;

	i = 0;
	while (i < l)
	{
		if (!is_valid(dst[i]))
		{
			j = 0;
			while (j < l)
			{
				free(dst[j]);
				dst[j] = NULL;
				j++;
			}
			free(dst);
			dst = NULL;
			end_program(a, b, 1);
		}
		i++;
	}
}

int	is_sorted(t_stack *stack)
{
	t_elem	*cur;

	cur = stack->head;
	while (cur->next != NULL && cur->value < cur->next->value)
		cur = cur->next;
	if (cur->next == NULL)
		return (1);
	else
		return (0);
}

int	is_double(t_stack *stack)
{
	t_elem	*cur;
	t_elem	*check;

	cur = stack->head;
	while (cur != NULL)
	{
		check = cur->next;
		while (check != NULL)
		{
			if (check->value == cur->value)
				return (1);
			check = check->next;
		}
		cur = cur->next;
	}
	return (0);
}

int	is_valid(char *new)
{
	char	*str;

	str = new;
	if (!str)// || (ft_strlen(str) == 0 && ft_strcmp(str, "")))
		return (0);
	if (ft_atoi_double(str) > INT_MAX || ft_atoi_double(str) < INT_MIN)
		return (0);
	while (*str)
	{
		if (*str == ' ' || ft_isdigit(*str))
			str++;
		else if (*str == '-' && ft_isdigit(*(str + 1))
			&& (str == new || *(str - 1) == ' '))
			str++;
		else if (*str == '+' && ft_isdigit(*(str + 1))
			&& (str == new || *(str - 1) == ' '))
			str++;
		else
			return (0);
	}
	return (1);
}
