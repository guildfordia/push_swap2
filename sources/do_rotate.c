/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_rotate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:29:08 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/11 15:29:10 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static void	rotate(t_elem **head, t_elem **last)
{
	if ((*head) == NULL || *last == NULL)
		return ;
	(*last)->next = (*head);
	(*head)->prev = (*last);
	(*head) = (*head)->next;
	(*head)->prev = NULL;
	(*last) = (*last)->next;
	(*last)->next = NULL;
}

void	do_rotate(char c, t_stack *a, t_stack *b)
{
	if (c == 'a')
	{
		rotate(&a->head, &a->last);
		write(1, "ra", 2);
	}
	else if (c == 'b')
	{
		rotate(&b->head, &b->last);
		write(1, "rb", 2);
	}
	end_action(a, b);
}
