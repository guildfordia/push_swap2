/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_small.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/22 15:22:52 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/11 18:32:15 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	sort_two(t_stack *a, t_stack *b, char c)
{
	if (a->head->value > a->head->next->value)
		do_swap(c, a, b);
}

void	sort_three(t_stack *a, t_stack *b, char c)
{
	int	d;
	int	e;
	int	f;

	d = a->head->value;
	e = a->head->next->value;
	f = a->head->next->next->value;
	if (d > e && e < f && d < f)
		do_swap(c, a, b);
	else if (d < e && e > f && d > f)
		do_rev_rotate(c, a, b);
	else if (d < e && e > f && d < f)
	{
		do_swap(c, a, b);
		do_rotate(c, a, b);
	}
	else if (d > e && e > f && d > f)
	{
		do_swap(c, a, b);
		do_rev_rotate(c, a, b);
	}
	else if (d > e && e < f && d > f)
		do_rotate(c, a, b);
}

void	sort_four(t_stack *a, t_stack *b, char c)
{
	int	min;

	min = find_smallest(a);
	smart_rotate(a, b, min, c);
	do_push('b', a, b);
	sort_three(a, b, c);
	do_push(c, a, b);
}

void	sort_five(t_stack *a, t_stack *b, char c)
{
	int	min;

	min = find_smallest(a);
	smart_rotate(a, b, min, c);
	do_push('b', a, b);
	sort_four(a, b, c);
	do_push(c, a, b);
}
