/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   smart_rotate.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:30:46 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/11 15:30:48 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static void	rotation(t_stack *a, t_stack *b, int rot, char c)
{
	if (rot < 0)
	{
		rot *= -1;
		while (rot > 0)
		{
			do_rev_rotate(c, a, b);
			rot--;
		}
	}
	else
	{
		while (rot > 0)
		{
			do_rotate(c, a, b);
			rot--;
		}
	}
}

static int	find_shortest(t_stack *stk, int nb)
{
	t_elem	*cur;
	int		rev;
	int		fwd;

	fwd = 0;
	rev = 1;
	cur = stk->head;
	while (cur->value != nb)
	{
		cur = cur->next;
		fwd++;
	}
	cur = stk->last;
	while (cur->value != nb)
	{
		cur = cur->prev;
		rev++;
	}
	if (rev < fwd)
		return (-rev);
	else
		return (fwd);
}

void	smart_rotate(t_stack *a, t_stack *b, int nb, char c)
{
	int		rot;
	t_stack	*stk;

	if (c == 'a')
		stk = a;
	else
		stk = b;
	rot = find_shortest(stk, nb);
	rotation(a, b, rot, c);
}
