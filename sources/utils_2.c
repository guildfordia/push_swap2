/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:31:53 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/11 15:53:29 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	list_length(t_stack *a)
{
	t_elem	*cur;
	int		len;

	len = 0;
	cur = a->head;
	while (cur != NULL)
	{
		len++;
		cur = cur->next;
	}
	return (len);
}

t_stack	*init_stack(void)
{
	t_stack	*stk;

	stk = (t_stack *)malloc(sizeof(t_stack));
	if (!stk)
		return (NULL);
	stk->head = NULL;
	stk->last = NULL;
	return (stk);
}

int	find_smallest(t_stack *stk)
{
	int		min;
	t_elem	*cur;

	min = INT_MAX;
	cur = stk->head;
	while (cur != NULL)
	{
		if (min > cur->value)
			min = cur->value;
		cur = cur->next;
	}
	return (min);
}

int	find_biggest(t_stack *stk)
{
	int		max;
	t_elem	*cur;

	max = INT_MIN;
	cur = stk->head;
	while (cur != NULL)
	{
		if (max < cur->value)
			max = cur->value;
		cur = cur->next;
	}
	return (max);
}

void	end_action(t_stack *a, t_stack *b)
{
	a->op++;
	write(1, "\n", 1);
	a->last = list_last(a);
	b->last = list_last(b);
}
