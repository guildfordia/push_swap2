/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/28 12:20:06 by alangloi          #+#    #+#             */
/*   Updated: 2021/08/27 14:47:57 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	sort_stack(int argc, t_stack *a, t_stack *b, char c)
{
	if (!is_double(a))
	{
		if (argc == 3)
			sort_two(a, b, c);
		else if (argc == 4)
			sort_three(a, b, c);
		else if (argc == 5)
			sort_four(a, b, c);
		else if (argc == 6)
			sort_five(a, b, c);
		else
			sort_big_stack(a, b);
	}
	else
		end_program(a, b, 1);
}

void	end_program(t_stack *a, t_stack *b, int i)
{
	while (a->head != NULL)
		del_top_elem(a);
	while (a->head != NULL)
		del_top_elem(b);
	free(a);
	free(b);
	a = NULL;
	b = NULL;
	if (i > 0)
	{
		write(2, "Error\n", 6);
		exit(1);
	}
	else
	{
		exit(0);
	}
}

static void	init_values(t_stack *a)
{
	a->op = 0;
	a->part = NULL;
	a->len = 0;
}

int	main(int argc, char **argv)
{
	t_stack	*a;
	t_stack	*b;
	int		j;

	if (argv == NULL)
		return (0);
	a = init_stack();
	b = init_stack();
	init_values(a);
	j = fill_list(a, b, argc, argv);
	argc += j;
	if (argc == 1)
		end_program(a, b, 0);
	else if (argc == 2 || is_sorted(a))
		end_program(a, b, 0);
	sort_stack(argc, a, b, 'a');
	end_program(a, b, 0);
	return (0);
}
