/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elements.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/30 18:19:21 by alangloi          #+#    #+#             */
/*   Updated: 2021/07/06 18:34:07 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	del_top_elem(t_stack *stk)
{
	t_elem	*tmp;

	tmp = stk->head;
	if (stk->head->next == NULL)
		stk->last = NULL;
	else
		stk->head->next->prev = NULL;
	stk->head = stk->head->next;
	free(tmp);
}

t_elem	*allocate_elem(int value)
{
	t_elem	*new;

	new = (t_elem *)malloc(sizeof(t_elem));
	if (!new)
		return (NULL);
	new->value = value;
	new->next = NULL;
	new->prev = NULL;
	return (new);
}

t_elem	*list_last(t_stack *stk)
{
	t_elem	*cur;

	if (!stk->head)
		return (NULL);
	cur = stk->head;
	while (cur->next != NULL)
		cur = cur->next;
	return (cur);
}

void	add_elem_front(t_stack *stk, t_elem *new)
{
	if (!stk->head && !new)
		return ;
	new->prev = NULL;
	new->next = NULL;
	if (stk->head)
	{
		stk->head->prev = new;
		new->next = stk->head;
	}
	stk->head = new;
}

void	add_elem_back(t_stack *stk, t_elem *new)
{
	if (!new)
		return ;
	new->next = NULL;
	new->prev = NULL;
	if (!stk->head)
	{
		add_elem_front(stk, new);
	}
	else
	{
		new->prev = stk->last;
		stk->last->next = new;
	}
	stk->last = new;
}
