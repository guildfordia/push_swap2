/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/28 12:18:50 by alangloi          #+#    #+#             */
/*   Updated: 2021/07/07 19:22:01 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "../libft/libft.h"
# include <stdio.h>
# include <limits.h>
# include <stdlib.h>

typedef struct s_elem
{
	int				value;
	struct s_elem	*next;
	struct s_elem	*prev;
}				t_elem;

typedef struct s_stack
{
	t_elem			*head;
	t_elem			*last;
	int				op;
	int				*part;
	int				len;
}				t_stack;

/* push swap */
void	sort_stack(int argc, t_stack *a, t_stack *b, char c);
void	end_program(t_stack *a, t_stack *b, int i);

/* sort big stack */
void	sort_big_stack(t_stack *a, t_stack *b);

/* sort small stack */
void	sort_three(t_stack *a, t_stack *b, char c);
void	sort_two(t_stack *a, t_stack *b, char c);
void	sort_four(t_stack *a, t_stack *b, char c);
void	sort_five(t_stack *a, t_stack *b, char c);

/* closest to top */
void	closest_to_top(t_stack *a, t_stack *b, int i);

/* prepare stack */
void	prepare_stack(t_stack *a, t_stack *b);

/* parts functions */
int		much_parts(int len);
int		*allocate_parts(t_stack *a, int nb_splits);
void	sort_parts(t_stack *a, t_stack *b, int i, int size);

/* smart rotate */
void	smart_rotate(t_stack *a, t_stack *b, int nb, char c);

/* fill list */
int		fill_list(t_stack *a, t_stack *b, int argc, char **argv);

/* partition */
int		*partition(t_stack *stk, int nb_splits);
int		*create_array(t_stack *stk);

/* actions */
void	do_rotate(char c, t_stack *a, t_stack *b);
void	do_rev_rotate(char c, t_stack *a, t_stack *b);
void	do_push(char c, t_stack *a, t_stack *b);
void	do_swap(char c, t_stack *a, t_stack *b);

/* elements */
t_elem	*allocate_elem(int value);
t_elem	*list_last(t_stack *stk);
void	add_elem_front(t_stack *stk, t_elem *new);
void	add_elem_back(t_stack *stk, t_elem *new);
void	del_top_elem(t_stack *stk);

/* utils_1 */
int		is_sorted(t_stack *stack);
int		is_double(t_stack *stack);
int		is_valid(char *str);
void	is_split(int l, char **dst, t_stack *a, t_stack *b);

/* utils_2 */
int		find_smallest(t_stack *stk);
int		find_biggest(t_stack *stk);
t_stack	*init_stack(void);
int		list_length(t_stack *a);
void	end_action(t_stack *a, t_stack *b);

/* ps split */
char	**ps_split(char const *s, char c, t_stack *a, t_stack *b);

/* debug */
void	print_list(char *str, t_stack *stk);
void	print_op(t_stack *a, t_stack *b);

#endif
