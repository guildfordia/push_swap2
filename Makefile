NAME = push_swap
CC = gcc
FLAGS = -Wall -Werror -Wextra
LIBFT = libft/libft.a
HEADER = includes
SRC = sources
OBJ = objects
SOURCES = push_swap.c \
          sort_small_stack.c \
		  sort_big_stack.c \
		  closest_to_top.c \
          prepare_stack.c \
          parts_functions.c \
          smart_rotate.c \
          fill_list.c \
          partition.c \
		  do_swap.c \
		  do_rev_rotate.c \
		  do_rotate.c \
		  do_push.c \
		  elements.c \
		  utils_1.c \
          utils_2.c \
          ps_split.c \
          debug.c
SRCS = $(addprefix $(SRC)/, $(SOURCES))
OBJS = $(addprefix $(OBJ)/, $(SOURCES:.c=.o))

all: $(OBJ) $(NAME)

$(OBJ):
	mkdir -p $(OBJ)

$(NAME): $(OBJS)
	make bonus -C libft
	$(CC) $(FLAGS) -o $(NAME) $(OBJS) $(LIBFT) -g

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) $(FLAGS) -o $@ -c $^ -I$(HEADER)

clean:
	make clean -C libft
	rm -rf $(OBJ)

fclean: clean
	make fclean -C libft
	rm -rf $(NAME)

re: fclean all

.PHONY: all fclean clean re
